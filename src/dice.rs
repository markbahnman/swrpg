// Copyright (c) 2018 Mark Bahnman
//
// Licensed under the MIT license <LICENSE or http://opensource.org/licenses/MIT>.
// All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.

//! `rpg` dice

use rand::prelude::*;

/// All the possible symbols which appear on dice
#[derive(Clone, Copy, Debug)]
pub enum Symbol {
    Advantage,
    Threat,
    Success,
    Failure,
    Triumph,
    Despair,
    Blank,
}

/// List of the different types of dice
#[derive(Debug)]
pub enum DieType {
    Boost,
    Setback,
    Proficency,
    Challenge,
    Ability,
    Difficulty,
}

/// Base struct which describes an instance of a die
#[derive(Clone, Debug, Default)]
pub struct Die {
    faces: Vec<(Symbol, Symbol)>,
    result: Option<(Symbol, Symbol)>,
}

impl Die {
    pub fn new(t: DieType) -> Die {
        match t {
            DieType::Boost => Die {
                faces: vec![
                    (Symbol::Blank, Symbol::Blank),
                    (Symbol::Blank, Symbol::Blank),
                    (Symbol::Success, Symbol::Blank),
                    (Symbol::Advantage, Symbol::Blank),
                    (Symbol::Advantage, Symbol::Success),
                    (Symbol::Advantage, Symbol::Advantage),
                ],
                ..Default::default()
            },
            DieType::Setback => Die {
                faces: vec![
                    (Symbol::Blank, Symbol::Blank),
                    (Symbol::Blank, Symbol::Blank),
                    (Symbol::Threat, Symbol::Blank),
                    (Symbol::Threat, Symbol::Blank),
                    (Symbol::Failure, Symbol::Blank),
                    (Symbol::Failure, Symbol::Blank),
                ],
                ..Default::default()
            },
            DieType::Proficency => Die {
                faces: vec![
                    (Symbol::Blank, Symbol::Blank),
                    (Symbol::Success, Symbol::Success),
                    (Symbol::Success, Symbol::Success),
                    (Symbol::Advantage, Symbol::Advantage),
                    (Symbol::Advantage, Symbol::Advantage),
                    (Symbol::Success, Symbol::Blank),
                    (Symbol::Success, Symbol::Blank),
                    (Symbol::Advantage, Symbol::Blank),
                    (Symbol::Advantage, Symbol::Success),
                    (Symbol::Advantage, Symbol::Success),
                    (Symbol::Advantage, Symbol::Success),
                    (Symbol::Triumph, Symbol::Blank),
                ],
                ..Default::default()
            },
            DieType::Challenge => Die {
                faces: vec![
                    (Symbol::Blank, Symbol::Blank),
                    (Symbol::Failure, Symbol::Failure),
                    (Symbol::Failure, Symbol::Failure),
                    (Symbol::Threat, Symbol::Threat),
                    (Symbol::Threat, Symbol::Threat),
                    (Symbol::Failure, Symbol::Blank),
                    (Symbol::Failure, Symbol::Blank),
                    (Symbol::Threat, Symbol::Blank),
                    (Symbol::Threat, Symbol::Failure),
                    (Symbol::Threat, Symbol::Failure),
                    (Symbol::Threat, Symbol::Failure),
                    (Symbol::Despair, Symbol::Blank),
                ],
                ..Default::default()
            },
            DieType::Difficulty => Die {
                faces: vec![
                    (Symbol::Blank, Symbol::Blank),
                    (Symbol::Failure, Symbol::Blank),
                    (Symbol::Threat, Symbol::Blank),
                    (Symbol::Threat, Symbol::Blank),
                    (Symbol::Threat, Symbol::Blank),
                    (Symbol::Threat, Symbol::Threat),
                    (Symbol::Threat, Symbol::Failure),
                    (Symbol::Failure, Symbol::Failure),
                ],
                ..Default::default()
            },
            DieType::Ability => Die {
                faces: vec![
                    (Symbol::Blank, Symbol::Blank),
                    (Symbol::Success, Symbol::Blank),
                    (Symbol::Success, Symbol::Blank),
                    (Symbol::Advantage, Symbol::Blank),
                    (Symbol::Advantage, Symbol::Blank),
                    (Symbol::Advantage, Symbol::Advantage),
                    (Symbol::Advantage, Symbol::Success),
                    (Symbol::Success, Symbol::Success),
                ],
                ..Default::default()
            },
        }
    }

    /// If a die has been rolled this will return the symbols on the face that was rolled
    pub fn get_result(&self) -> Option<(Symbol, Symbol)> {
        self.result
    }

    /// Roll the dice and set the result
    pub fn roll(&mut self) {
        let length = self.faces.len();
        let mut rng = thread_rng();
        let res: usize = rng.gen_range(0, length);

        self.result = Some(self.faces[res].clone());
    }
}
