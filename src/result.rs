// Copyright (c) 2018 Mark Bahnman
//
// Licensed under the MIT license <LICENSE or http://opensource.org/licenses/MIT>.
// All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.

//! `rpg` result

use std::fmt;

use dice::{Die, Symbol};

/// Bulider for a result
#[derive(Default)]
pub struct ResultBuilder {
    dice: Option<Vec<Die>>,
    successes: u8,
    failures: u8,
    threat: u8,
    advantage: u8,
    triumphs: u8,
    despairs: u8,
}

impl ResultBuilder {
    pub fn new() -> Self {
        ResultBuilder {
            ..Default::default()
        }
    }

    /// Add a die to the result
    pub fn add_die(&mut self, die: Die) {
        match die.get_result() {
            Some((first_symbol, second_symbol)) => {
                &[first_symbol, second_symbol].iter().for_each(|d| {
                    match d {
                        Symbol::Success => self.successes += 1,
                        Symbol::Failure => self.failures += 1,
                        Symbol::Advantage => self.advantage += 1,
                        Symbol::Threat => self.threat += 1,
                        Symbol::Triumph => {
                            self.triumphs += 1;
                            self.successes += 1;
                        }
                        Symbol::Despair => {
                            self.despairs += 1;
                            self.failures += 1;
                        }
                        Symbol::Blank => (),
                    };
                });

                match self.dice {
                    Some(ref mut dice) => dice.push(die),
                    None => self.dice = Some(vec![die]),
                }
            }
            None => (),
        }
    }

    /// Build the Result from the Builder struct
    pub fn build(&self) -> Result {
        Result {
            dice: self.dice.clone().unwrap(),
            successes: self.successes,
            failures: self.failures,
            threat: self.threat,
            advantage: self.advantage,
            triumphs: self.triumphs,
            despairs: self.despairs,
        }
    }
}

#[derive(Debug)]
pub struct Result {
    dice: Vec<Die>,
    pub successes: u8,
    pub failures: u8,
    pub threat: u8,
    pub advantage: u8,
    pub triumphs: u8,
    pub despairs: u8,
}

impl Result {
    pub fn is_success(&self) -> bool {
        self.successes > self.failures
    }

    pub fn get_net_success_or_failure(&self) -> u8 {
        let net = self.successes as i8 - self.failures as i8;
        net.abs() as u8
    }

    pub fn get_net_threat_or_advantage(&self) -> u8 {
        let net = self.advantage as i8 - self.threat as i8;
        net.abs() as u8
    }

    pub fn has_advantage(&self) -> bool {
        self.advantage > 0
    }

    pub fn has_threat(&self) -> bool {
        self.threat > 0
    }
}

impl fmt::Display for Result {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut lines = Vec::new();

        if self.is_success() {
            lines.push(format!(
                "Roll was a success with {} successes",
                self.get_net_success_or_failure()
            ));
        } else {
            let net_failures = self.get_net_success_or_failure();

            if net_failures == 0 {
                lines.push(String::from(
                    "Roll was a failure with no net successes or failures",
                ));
            } else {
                lines.push(format!(
                    "Roll was a failure with {} failures",
                    self.get_net_success_or_failure()
                ));
            }
        }

        if self.has_advantage() {
            lines.push(format!("{} advantage", self.get_net_threat_or_advantage()));
        } else if self.has_threat() {
            lines.push(format!("{} threat", self.get_net_threat_or_advantage()));
        } else {
            lines.push(String::from("no threat or advantage"));
        }

        if self.triumphs > 0 && self.despairs > 0 {
            lines.push(format!(
                "{} triumph and {} despair",
                self.triumphs, self.despairs
            ));
        } else if self.triumphs > 0 {
            lines.push(format!("and {} triumph", self.triumphs));
        } else if self.despairs > 0 {
            lines.push(format!("and {} despair", self.despairs));
        }

        write!(f, "{}", lines.join(", "))
    }
}
