// Copyright (c) 2018 Mark Bahnman
//
// Licensed under the MIT license <LICENSE or http://opensource.org/licenses/MIT>.
// All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.

//! `rpg` runtime

use clap::{App, Arg};
use dice_pool::PoolBuilder;
use error::Result;
use std::io::{self, Write};

/// CLI Runtime
pub fn run() -> Result<i32> {
    let _matches = App::new(env!("CARGO_PKG_NAME"))
        .version(env!("CARGO_PKG_VERSION"))
        .author(env!("CARGO_PKG_AUTHORS"))
        .about("Prints 'Hello, Rustaceans!' to stdout")
        .arg(
            Arg::with_name("ability")
                .long("ability")
                .short("a")
                .value_name("ABILITY_VALUE")
                .help("Number of Ability dice to use")
                .takes_value(true)
                .required(true),
        )
        .arg(
            Arg::with_name("difficulty")
                .long("difficulty")
                .short("d")
                .value_name("CHECK_DIFFICULTY")
                .help("Number of difficulty dice to use")
                .takes_value(true)
                .default_value("0"),
        )
        .arg(
            Arg::with_name("challenge")
                .long("challenge")
                .short("c")
                .value_name("CHALLENGE_DICE")
                .help("Number of dice to upgrade to challenge dice")
                .takes_value(true)
                .default_value("0"),
        )
        .arg(
            Arg::with_name("proficency")
                .long("proficency")
                .short("p")
                .value_name("PROFICENCY_DICE")
                .help("Number of proficency dice to use")
                .takes_value(true)
                .default_value("0"),
        )
        .arg(
            Arg::with_name("setback")
                .long("setback")
                .short("s")
                .value_name("SETBACK_DICE")
                .help("Number of setback dice to use")
                .takes_value(true)
                .default_value("0"),
        )
        .arg(
            Arg::with_name("boost")
                .long("boost")
                .short("b")
                .value_name("BOOST_DICE")
                .help("Number of boost dice to use")
                .takes_value(true)
                .default_value("0"),
        )
        .get_matches();

    let difficulty = value_t_or_exit!(_matches.value_of("difficulty"), u8);
    let proficency = value_t_or_exit!(_matches.value_of("proficency"), u8);
    let challenge = value_t_or_exit!(_matches.value_of("challenge"), u8);
    let setback = value_t_or_exit!(_matches.value_of("setback"), u8);
    let boost = value_t_or_exit!(_matches.value_of("boost"), u8);
    let ability = value_t_or_exit!(_matches.value_of("ability"), u8);

    // Build the pool of dice, roll them, and return the result
    let result = PoolBuilder::new()
        .difficulty(difficulty)
        .challenge(challenge)
        .setback(setback)
        .boost(boost)
        .ability(ability)
        .proficency(proficency)
        .roll();

    writeln!(io::stdout(), "{}", result)?;
    Ok(0)
}
