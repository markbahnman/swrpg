// Copyright (c) 2018 Mark Bahnman
//
// Licensed under the MIT license <LICENSE or http://opensource.org/licenses/MIT>.
// All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.

//! `rpg` errors
error_chain!{
    foreign_links {
        Io(::std::io::Error);
    }
}
