// Copyright (c) 2018 Mark Bahnman
//
// Licensed under the MIT license <LICENSE or http://opensource.org/licenses/MIT>.
// All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.

//! `rpg` 0.1.0
#![deny(missing_docs)]
#[macro_use]
extern crate error_chain;
#[macro_use]
extern crate clap;
extern crate rand;

mod dice;
mod dice_pool;
mod error;
mod result;
mod run;

use std::io::{self, Write};
use std::process;

/// CLI Entry Point
fn main() {
    match run::run() {
        Ok(i) => process::exit(i),
        Err(e) => {
            writeln!(io::stderr(), "{}", e).expect("Unable to write to stderr!");
            process::exit(1)
        }
    }
}
