// Copyright (c) 2018 Mark Bahnman
//
// Licensed under the MIT license <LICENSE or http://opensource.org/licenses/MIT>.
// All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.

//! `rpg` dice pool

use dice::{Die, DieType};
use result::{Result, ResultBuilder};

pub struct PoolBuilder {
    difficulty: Option<Vec<Die>>,
    ability: Option<Vec<Die>>,
    proficency: Option<Vec<Die>>,
    boost: Option<Vec<Die>>,
    setback: Option<Vec<Die>>,
    challenge: Option<Vec<Die>>,
}

impl PoolBuilder {
    pub fn new() -> Self {
        PoolBuilder {
            difficulty: None,
            ability: None,
            proficency: None,
            boost: None,
            setback: None,
            challenge: None,
        }
    }

    pub fn setback(mut self, setback: u8) -> Self {
        if setback > 0 {
            self.difficulty = Some(vec![Die::new(DieType::Setback); setback as usize]);
        }
        self
    }

    pub fn challenge(mut self, challenge: u8) -> Self {
        if challenge > 0 {
            self.challenge = Some(vec![Die::new(DieType::Challenge); challenge as usize]);
        }
        self
    }

    pub fn boost(mut self, boost: u8) -> Self {
        self.boost = Some(vec![Die::new(DieType::Boost); boost as usize]);
        self
    }

    pub fn proficency(mut self, proficency: u8) -> Self {
        self.proficency = Some(vec![Die::new(DieType::Proficency); proficency as usize]);
        self
    }

    pub fn ability(mut self, ability: u8) -> Self {
        self.ability = Some(vec![Die::new(DieType::Ability); ability as usize]);
        self
    }

    pub fn difficulty(mut self, difficulty: u8) -> Self {
        self.difficulty = Some(vec![Die::new(DieType::Difficulty); difficulty as usize]);
        self
    }

    /// Roll the pool of dice and get the result
    pub fn roll(self) -> Result {
        let mut pool: Vec<Die> = Vec::new();

        // TODO: is extending the best option for concating vectors?
        if let Some(ability) = self.ability {
            pool.extend(ability);
        }

        if let Some(challenge) = self.challenge {
            pool.extend(challenge);
        }

        if let Some(difficulty) = self.difficulty {
            pool.extend(difficulty);
        }

        if let Some(proficency) = self.proficency {
            pool.extend(proficency);
        }

        if let Some(boost) = self.boost {
            pool.extend(boost);
        }

        if let Some(setback) = self.setback {
            pool.extend(setback);
        }

        // TODO: this seems ugly, must be a better way to do this
        let results = pool
            .iter()
            .map(|d| {
                let mut nd = d.clone();
                nd.roll();
                nd
            })
            .fold(ResultBuilder::new(), |mut result, die| {
                result.add_die(die);
                result
            });

        results.build()
    }
}
