.PHONY: build clean clean-runtime run test watch \
				check-tool-cargo check-tool-cargo-watch \
				get-version

all: build

has-cargo:
	which cargo &&

VERSION=$(shell awk '/version\s+=\s+\"([0-9|\.]+)\"$$/{print $$3}' Cargo.toml)

CARGO := $(shell command -v cargo 2> /dev/null)
CARGO_WATCH := $(shell command -v cargo-watch 2> /dev/null)

get-version:
	@echo -e ${VERSION}

check-tool-cargo:
ifndef CARGO
	$(error "`cargo` is not available please install cargo (https://github.com/rust-lang/cargo/)")
endif

check-tool-cargo-watch:
ifndef CARGO_WATCH
	$(error "`cargo-watch` is not available please install cargo-watch (https://github.com/passcod/cargo-watch)")
endif

clean: check-tool-cargo
	cargo clean

build: check-tool-cargo
	cargo build

release: check-tool-cargo
	cargo build --release --verbose --jobs 1

run:
	cargo run

test:
	cargo test

watch: check-tool-cargo check-tool-cargo-watch
	cargo watch -x build
